//We can also have an array of objects. We can group together objects in an array. That array can also use array methods. for the objects. Howvever, being objects are more complex than strings or numbers, there is a difference when handling them.

let users = [

	{
		name: "Mike Shell",
		username: "mikeBlueShell",
		email: "mikeyShell01@gmail.com",
		password: "iammikey1999",
		isActive: true,
		dateJoined: "August 8, 2011"
	},
	{
		name: "Jake Janella",
		username: "jjnella99",
		email: "jakejanella_99@gmail.com",
		password: "jakiejake12",
		isActive: true,
		dateJoined: "January 14, 2015"
	},

];

//much like a regular array, we can actually access this array of objects the same way
console.log(users[0]);

//Accessing properties of objects in an array:
console.log(users[1].email);

//updating the properties of objects in an array:
	//mini-activity - update emails of user 1 and 2
	users[0].email = "mikeKingofShells@gmail.com";
	users[1].email = "janellajakeArchitect@gmail.com";

	console.log(users[0].email);
	console.log(users[1].email);

//it is also possible to use array methods for array of objects
	//adding another user into the array:

	users.push({

		name: "James Jameson",
		username: "iHateSpidey",
		email: "jamesJjameson@gmail.com",
		password: "spideyismenace64",
		isActive: true,
		dateJoined: "February 14, 2000"

	})

//alternatively, you can use a constructor	

	class User{

		constructor(name,username,email,password){
			this.name = name;
			this.username = username;
			this.email = email;
			this.password = password;
			this.isActive = true;
			this.dateJoined = "September 28, 2021";
		}
	}	

	let newUser1 = new User("Kate Middletown","notTheDuchess","seriouslyNotDuchess@gmail.com","notRoyaltyAtAll");	

	console.log(newUser1);

	users.push(newUser1);

	console.log(users);

//find()
function login(username,password){
	//check if username does exist or any user uses that username
	//check if the password given matches the password of our user in the array.
	//note: .includes is only good for primitive data
	let userFound = users.find((user)=>{
		//user is a parameter which receives each items in the users array and the users array is an array of objects. Therefore, we can expect that the user parameter will contain an object.
		return user.username === username && user.password === password
			//ends as soon as it finds one that matches. returns undefined then it doesnt get a match.
	});

	console.log(userFound);

	//mini-activity. alerts if correct login or not
	/*if typeof userFound === undefined {
		alert("Login Failed, Invalid Credentials.")
	} else{
		alert(`Thank you for logging in, ${username}`)
	}
	*/

	if (userFound) {	
		alert(`Thank you for logging in, ${username}`)
	} else{
		alert("Login Failed, Invalid Credentials.")
	}


}

// login("notTheDuchess","notRoyaltyAtAll");

//activity

//create function

let courses = [];

const postCourse = (id,name,description,price) => {

	class Course{
			constructor(id,name,description,price){
				this.id = id;
				this.name = name;	
				this.description = description;
				this.price = price;
				this.isActive = true;
			}
	}
	//note: Class constructor is better put outside the function - making it global would make it reusable.

	let newCourse = new Course(id,name,description,price);
	courses.push(newCourse);

	alert(`You have created ${name}. The price is ${price}`)

};


//retrieve/read
	const getSingleCourse = (course) => {
		let idFound = courses.find((course) => {
			return course.id === course; 	
		});

		console.log(idFound);
		return(idFound);

	}

//delete
	const deleteCourse = () => {
		courses.pop();
		//note: .pop() returns a value	
	}


//testing (add 1 course, view course, then delete.)
postCourse("0449","Philo1","Intro to Philosophy",40);

getSingleCourse("0449");

deleteCourse();

console.log(courses);





